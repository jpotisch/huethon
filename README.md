# Huethon

## What?
A simple Python program to add a physical light switch to Philips Hue lights. Hue + Python = Huethon.

## Why?
Smart lights are handy for scheduling, working with sensors, etc. but sometimes you just want to turn the light on or off without having to take out your phone, yell at a device, etc.

## How?
Get a Raspberry Pi or similar cheap computer amenable to hardware hacking. I used a Raspberry Pi Zero W that I was already using to run [Pi-Hole](https://pi-hole.net/), soldered a momentary contact switch to the GPIO pins and mounted the Pi inside and the button on top of a small enclosure. Run Huethon as a [systemd](https://en.wikipedia.org/wiki/Systemd) so it runs automatically at startup and you're done!

### Preparing the hardware
Wire one lead from the GPIO pin you wish to use and another one to one of the [5v pins](https://pinout.xyz/pinout/pin2_5v_power). I chose [BCM pin 4](https://pinout.xyz/pinout/pin7_gpio4) but any of the [BCM GPIO](https://pinout.xyz/pinout/wiringpi) pins are fine. Just update `bcmPinNumber` in `config.ini` accordingly. This tells the Pi that the pin will be "low" (0v) when the switch is open (unclicked) and "high" (5v) when closed (clicked), so the program will treat voltage transitions from low to high as the trigger to change the light brightness.

### Preparing the software

1. Copy or rename `huethon-sample.ini` to `huethon.ini`.
2. Follow the [Philips Hue API instructions](https://developers.meethue.com/develop/get-started-2/#so-lets-get-started) to create a username on your Hue bridge and use it to set the `username` value in `huethon.ini`. This is unique to your bridge and is used to authenticate all API calls.
3. If you used a different pin than BCM 4 when setting up your hardware (see above), update its value in `bcmPinNumber` in `huethon.ini`.
4. Set the value of `lightNumber`

The following instructions assume you are using a Raspberry Pi and have already copied this repo to the `/home/pi/huethon` folder on it. If you copied it to a different folder you'll need to update the huethon.service file accordingly.

Once your hardware is ready, run the program manually and confirm nothing blows up. If all goes well you should see the following:

```
pi@pi-hole:~/huethon $ python3 main.py
Welcome to Huethon! Connecting to hardware . . .
Connecting to Hue light 4 . . .
Huethon ready and waiting for button presses on pin 4.
```

Now click your button and you should see the brightness change. Keep clicking and you should see high > medium > low > off > high, etc. Each click should add to the program output, e.g.

```
Light 4's brightness set to 255
Light 4's brightness set to 90
Light 4's brightness set to 10
Light 4's brightness set to 0
Light 4's brightness set to 255
```

#### Setting up systemd

When you are satisfied that Huethon and your hardware are working well together, you'll want to configure systemd to make Huethon run automatically when your Raspberry Pi starts up or reboots, and to restart automatically if Huethon crashes.

```
sudo cp huethon.service /etc/systemd/system  # Make service file available to systemd
sudo systemctl enable huethon --now          # Start Huethon on boot, and start it now
systemctl is-active huethon                  # Returns 'active' if previous step worked
```

