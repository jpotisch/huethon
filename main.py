import configparser
import signal
import sys
import RPi.GPIO as GPIO
from qhue import Bridge

cp = configparser.ConfigParser()
cp.read('huethon.ini')
config = cp['default']


def cleanup(sig, frame):
    GPIO.cleanup()
    print('Goodbye!')
    sys.exit(0)


def button_pressed_callback(channel):
    if not GPIO.input(config.getint('bcmPinNumber')):
        return

    state = light()['state']

    # Get current brightness. If light is off, always treat brightness as zero.
    oldBrightness = state['bri'] if state['on'] else 0

    # Cycle through brightness levels to 0, then back to highest brightness.
    newBrightness = getNewBrightness(oldBrightness, list(map(int, config['brightnessLevels'].split(','))))

    # Now try to set the new brightness level, bailing out on error
    try:
        # API won't set brightness if light is off, so handle off differently
        if(newBrightness == 0):
            light.state(on=False)
        else:
            light.state(on=True, bri=newBrightness)
    except Exception as err:
        print(f'ERROR: Could not set light ' +
              f'{config["lightNumber"]}\'s state: {err}')
        exit(1)

    # Report back on what we did
    print(f"Light {config['lightNumber']}'s brightness set to {newBrightness}")


def getNewBrightness(oldBrightness, levels):
    """
    Given a current brightness and an array of target brightness levels,
    return next brightness to cycle to (high to low to off to high etc.)
    """
    numLevels = len(levels)
    levels = levels + [0]
    for x in range(0, numLevels):
        # Find nearest level to current brightness and return one lower
        if oldBrightness >= (levels[x] + levels[x + 1]) / 2:
            return levels[x + 1]

    # Didn't find level below current one, so light is nearly or totally off.
    # If nearly off, turn off. If off, loop around to max brightness.
    return levels[0] if oldBrightness == 0 else 0


if __name__ == '__main__':
    print('Welcome to Huethon! Connecting to hardware . . .')

    pin = config.getint('bcmPinNumber')
    lightNumber = config.getint('lightNumber')
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(config.getint('bcmPinNumber'), 
               GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

    # Find bridge and light (get state just to confirm this works)
    print(f'Connecting to Hue light {lightNumber} . . .')
    try:
        light = Bridge(config['bridgeAddress'], config['username']) \
                        .lights[lightNumber]
        state = light()['state']
    except Exception as err:
        print(f'ERROR: Could not get light {lightNumber}\'s state: {err}')
        exit(1)

    GPIO.add_event_detect(config.getint('bcmPinNumber'),
                          GPIO.RISING, callback=button_pressed_callback,
                          bouncetime=100)
    print(f'Huethon ready and waiting for button presses on pin {pin}.')

    signal.signal(signal.SIGINT, cleanup)
    signal.pause()
